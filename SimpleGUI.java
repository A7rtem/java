package ru.artem.gui;
import java.awt.*; 
import java.awt.event.*;
import java.lang.*;
import javax.swing.*;
import java.io.*;
import java.io.IOException;

public class SimpleGUI  extends JFrame {
 private JButton button = new JButton("finish");
 private JButton button0 = new JButton("0");
 private JButton button1 = new JButton("1");
 private JButton button2 = new JButton("2");
 private JButton button3 = new JButton("3");
 private JButton button4 = new JButton("4");
 private JButton button5 = new JButton("5");
 private JButton button6 = new JButton("6");
 private JButton button7 = new JButton("7");
 private JButton button8 = new JButton("8");
 private JButton button9 = new JButton("9");
 private JButton buttonDote = new JButton(".");
 private JButton buttonZ1 = new JButton("(");
 private JButton buttonZ2 = new JButton(")");
 private JButton buttonMul = new JButton("*");
 private JButton buttonDivision = new JButton("/");
 private JButton buttonSqr = new JButton("sqr");
 private JButton buttonClear = new JButton("Clear");
 private JButton buttonmines = new JButton("-");
 private JButton buttonPlus = new JButton("+");
 private JButton buttonSIN = new JButton("SIN(grad)");
 private JTextField input = new JTextField("",200);
 String s12 =new String ("") ;

 public SimpleGUI (){
  super("Simple Example");
  this.setBounds(100, 100, 500,300);
  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  Container container = this.getContentPane();
  container.setLayout(new GridLayout(7, 3, 2, 2));
  ButtonGroup group1 = new ButtonGroup();
  group1.add(button0);
  group1.add(button1);
  group1.add(button2);
  group1.add(button3);
  group1.add(button4);
  group1.add(button5);
  group1.add(button6);
  group1.add(button7);
  group1.add(button8);
  group1.add(button9);
  group1.add(buttonDote);
  group1.add(buttonZ1);
  group1.add(buttonZ2);
  group1.add(buttonMul);
  group1.add(buttonDivision);
  group1.add(buttonClear);
  group1.add(buttonPlus);
  group1.add(buttonmines);
  group1.add(buttonSIN);
  group1.add(buttonSqr);
  group1.add(button);
  container.add(button0);
  container.add(button1);
  container.add(button2);
  container.add(button3);
  container.add(button4);
  container.add(button5);
  container.add(button6);
  container.add(button7);
  container.add(button8);
  container.add(button9);
  container.add(buttonDote);
  container.add(buttonZ1);
  container.add(buttonZ2);
  container.add(buttonMul);
  container.add(buttonDivision);
  container.add(buttonClear);
  container.add(buttonmines);
  container.add(buttonPlus);
  container.add(buttonSIN);
  container.add(buttonSqr);
  container.add(button);
  container.add(input);
  button.addActionListener(new ButtonEventListener());
  button0.addActionListener(new ButtonEventListener0());
  button1.addActionListener(new ButtonEventListener1());
  button2.addActionListener(new ButtonEventListener2());
  button3.addActionListener(new ButtonEventListener3());
  button4.addActionListener(new ButtonEventListener4());
  button5.addActionListener(new ButtonEventListener5());
  button6.addActionListener(new ButtonEventListener6());
  button7.addActionListener(new ButtonEventListener7());
  button8.addActionListener(new ButtonEventListener8());
  button9.addActionListener(new ButtonEventListener9());
  buttonDote.addActionListener(new ButtonEventListenerDote());
  buttonZ1.addActionListener(new ButtonEventListenerZ1());
  buttonZ2.addActionListener( new ButtonEventListenerZ2());   
  buttonMul.addActionListener(new ButtonEventListenerMul());   
  buttonDivision.addActionListener(new ButtonEventListenerDivision());   
  buttonClear.addActionListener(new ButtonEventListenerClear());        
  buttonmines.addActionListener(new ButtonEventListenerMines());
  buttonPlus.addActionListener(new ButtonEventListenerPlus());
  buttonSIN.addActionListener(new ButtonEventListenerSIN());
  buttonSqr.addActionListener(new ButtonEventListenerSqr());
 }
        
class ButtonEventListener implements ActionListener
 {
 boolean b=false;//используется в методах связанных со скобкам//используется в методах связанных со скобкамии bracketSearch() deletBracket()
 int iii=0;// методы StToArr,sort
 String[] oper = new String[50];//используется везде кроме метода sort()
 int ind1=0,ind3=0, ind0=0;
 int ind2=oper.length-1;
 String str1 =new String ("") ; 

@Override
 public void actionPerformed (ActionEvent e) {   
//сообщаем в консоль о нажатой клавише "finish"
  System.out.println(s12+" ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,new operasion");
//bild array from s12
  StToArr();

// find bracket  
  bracketSearch();
//выполняем операции по приоритету (внутри скобок если есть) 
  schot();
//set oper value to input./ в инпут пишем значение нулевого элемента массива 
  input.setText(oper[0]);
//присваиваем s12 значение вывода
  s12 = input.getText();
//выводим в консоль s12
  System.out.println(s12);
    }

 void StToArr() 
  {
  int len = 0;
  int longread =0;
  char[] temp;
  boolean a=false;
   System.out.println("метод StToArr");
  // заполняем массив нулями
  for (int i=0 ; (i<oper.length) ; i++)
   { 
   oper[i]="0";
  }
  //////////////////////////// 
  ind1=0;//нулевой элемент
  ind2=oper.length-1;//конечный элемент
  a=true;
  iii=0;
  s12 = input.getText();
  System.out.println(s12+"-s12 перед циклом while ");
  SetSpise ab = new SetSpise("");
  
  System.out.println(s12);
  while (a){System.out.println("in while(a)");
   ++iii;
   s12 = input.getText(); 
   s12=(ab.setSpise(s12));  
   temp = s12.toCharArray();
   len = s12.length();
   longread =s12.indexOf(' ');
   if (len==0){// если длинна s12 0 
    System.out.println("пУстой ввод");
    a=false;
    return;
   }
   else if (longread==0){// если  пробел на первом месте- сокращаем s12 на 1 элемент
    System.out.println(" пробел на первом месте");
    s12 ="";
    for (int ii=1;len!=ii;++ii) {
     s12+=temp[ii];
    }
    input.setText(s12);
    --iii;
   }
   else {    // если пробел не найден на первом месте    
    if (longread==-1){ //если нет пробелов в s12, остался последний элемент
     System.out.println("Не осталось пробелов") ;
     System.out.print("all long(len)- "+len+"\n"+"до пробела(longread) "+longread+"\n"); 
     System.out.println(s12+" -до изменений"); 
     str1="";
     for (int ii=0;len!=ii;++ii){//s12 перемещаем в str1
      str1+=temp[ii];
     }
     System.out.println (str1+" -полученый элемент");
     System.out.println("ввод пуст");   
     System.out.println( sort());
     s12="";// очищаем s12
     input.setText(s12);  // устанавливаем в окно ввода s12
     a=false;// выход из цыкла
     System.out.println("\n array to bilded"); // 
     // отображаем элемнеты   //
     for (int i=0;i<(oper.length);i++){        //
      System.out.println ("oper["+i+"]"+oper[i]);//
     }                                                                   
     System.out.println("//далее счёт .............."); 
    }
    else {            //если в s12 есть не один элемент (есть пробелы)    
     System.out.print("all long(len)- "+len+"\n"+"до пробела(longread) "+longread+"\n"); 
     System.out.println(s12+" -до изменений");
     str1="";// в str1 грузим первый элемент
     for (int ii=0;longread!=ii;++ii)
      {
      str1+=temp[ii];
     }
     ///////////////////////////////                        
     System.out.println (str1+" -полученый элемент");
     System.out.println( sort());
     ///////WRITE TO s12 остаток
     s12 ="";
     for (int ii=longread;len!=ii;++ii)
      {
      s12+=temp[ii];
     }
     ///////////////////////////////            
     System.out.println(s12+" -после изменений");
     input.setText(s12);                                                
    }                                                                                         
   }
  }
 }
    
 String sort(){
  System.out.println("in sort");
  System.out.println(str1+"  "+iii);
  oper [iii-1]= str1;
  ind3=iii-1;//позиция конца массива
  System.out.println("in sort ind3 ="+ind3);
 String s="end sort"; 
 return s;
 }

 void bracketSearch(){
  if (ind2==49){
   //++ind3;
   ind2=ind3;  
  }
  System.out.println("metod bracketSearch()");
  System.out.println("ind1 ="+ind1);
  System.out.println("ind2 ="+ind2);
  //ищем скобки( для сокращения области поиска операцый)
  //проверяем нетли  скобок (внутри области действия) (если есть запускаем рекурсивно пока внутри не останется скобок )
  for (int i=ind1+1;i<(ind2);++i){//System.out.println("поиск открывающей скобки внутри области поиска");
   if (oper[i].equals("(")){   System.out.println("найдена открывающая скобка внутри");
    System.out.println("ind1 -"+ind1+"  изменено на ="+i);
    ind1=i;
    System.out.println("ind1 nearby for "+ind1); 
    // boolean a2 =true ;
    for(int zii=(ind1);zii<=ind3;++zii)   { // ищем закрывающую скобку
     System.out.println("ind2 in for "+ind2+"\nind1 in for "+ind1);
     if  (oper[zii].equals("(")){
      ind1=(zii);
     }
     if (oper[zii].equals(")")){
      ind2=(zii);
      System.out.println(")"+" найдена\n\n\n");
      //   System.out.println("ind1 "+ind1+" ind2 "+ind2);
      // a2=false;// выход из цыкла
      System.out.println("ind2 in for "+ind2+"\nind1 in for "+ind1);
      b=true;
      return;
     }
    }
    System.out.println("ind1 "+ind1+" ind2 "+ind2+" повышен приоритет в облаcтях "); 
   }
  } 
  System.out.println("нет скобок внутри области поиска (открывающей)");
  if (oper[ind1].equals("(")){ //если область поиска внутри скобок и другой открывающей скобки не нашлось
   for(int zii=(ind1);zii<=ind3;++zii)   { 
    System.out.println("ind2 in for "+ind2+"\nind1 ind2=(zii);in for "+ind1);
    if (oper[zii].equals(")")){// ищем закрывающую скобку
     ind2=(zii);
     //   System.out.println("ind1 "+ind1+" ind2 "+ind2);
     // a2=false;// выход из цыкла
     System.out.println("ind2 in for "+ind2+"\nind1 in for "+ind1);
     b=true;
     return;
    }  
   }           
   deletBracket();
  }
  System.out.println("ind1 ="+ind1);
  System.out.println("ind2 ="+ind2);
 } 

 void schot() { System.out.println("метод schot() ");
  if (ind2-ind1<3&&b){ // в скобках 1 позиция
   System.out.println("перед удалением скобок,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
   setOper();
   vivod();
   deletBracket();//сносит скобки
   setOper();
   vivod();
   StToArr(); 
   bracketSearch();
   schot();           
  }
  ///дальше по приоритету операцый проверяем вложенный массив
  for (int i=ind1;i<(ind2);++i)//System.out.println("поиск SIN , COS, TG,  ");
   {
   //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   if (oper[i].equals("SIN")){System.out.println("найдена операция Sin");
    ind0=i;
    if(b){//если нашлась хотябы 1 скобка 
     //   plas0();//работает в скобках  
      if (ind2-ind1<3){ // в скобках 1 позиция
       System.out.println("перед удалением скобок,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
       setOper();
       vivod();
       deletBracket();//сносит скобки
       setOper();
       vivod();
      }
      SIN();
      setOper();
      vivod();
      StToArr();
      bracketSearch();
      schot();           
      /* for (int id=oper.length-1;id>ind1;--id){//cдвиг массива для установки скобок 
      oper[id]=oper[id-1];
      }
      oper[ind1]="(";//установка скобок
      oper[ind1+4]=")";
      schot();
      */
     }
     else{//
      SIN();// не нужно удалять  скобки( работает если скобок нет, не сносит скобки)
      vivod();
      setOper(); 
      StToArr();
      bracketSearch();
      schot();
      //              */
     }
    }

   //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   if (oper[i].equals("Sqr")){System.out.println("найдена операция Sqr");
    ind0=i;
    if(b){//если нашлась хотябы 1 скобка 
     //   plas0();//работает в скобках  
      if (ind2-ind1<3){ // в скобках 1 позиция
       System.out.println("перед удалением скобок,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
       setOper();
       vivod();
       deletBracket();//сносит скобки
       setOper();
       vivod();
      }
      Sqr();
      setOper();
      vivod();
      StToArr();
      bracketSearch();
      schot();           
      /* for (int id=oper.length-1;id>ind1;--id){//cдвиг массива для установки скобок 
      oper[id]=oper[id-1];
      }
      oper[ind1]="(";//установка скобок
      oper[ind1+4]=")";
      schot();
      */
     }
     else{//
      Sqr();// не нужно удалять  скобки( работает если скобок нет, не сносит скобки)
      vivod();
      setOper(); 
      StToArr();
      bracketSearch();
      schot();
      //              */
     }
    }
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   
   } 
   for (int i=ind1;i<(ind2);++i)//System.out.println("поиск *  ");
    {
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if (oper[i].equals("*")){System.out.println("найдена операция *");
     System.out.println(ind0+"=ind0 "+ind2+"=ind2");
     ind0=i-2;
     System.out.println(ind0+"=ind0 "+ind2+"=ind2");            
     if(b){//если нашлась хотябы 1 скобка 
      //   plas0();//работает в скобках  
      if (ind2-ind1<3){ // в скобках 1 позиция
       System.out.println("перед удалением скобок,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
       setOper();
       vivod();
       deletBracket();//сносит скобки
       setOper();
       vivod();
      }
      mul();
      setOper();
      vivod();
      StToArr();
      bracketSearch();
      schot();           
      /* for (int id=oper.length-1;id>ind1;--id){//cдвиг массива для установки скобок 
      oper[id]=oper[id-1];
      }
      oper[ind1]="(";//установка скобок
      oper[ind1+4]=")";
      schot();
      */
     }
     else{//
      //      /*
      mul();// не нужно удалять  скобки( работает если скобок нет, не сносит скобки)
      vivod();
      setOper(); 
      StToArr();
      bracketSearch();
      schot();
      //              */
     }
    }
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   
    if (oper[i].equals("/")){System.out.println("найдена операция /");
     System.out.println(ind0+"=ind0 "+ind2+"=ind2");
     ind0=i-2;
     System.out.println(ind0+"=ind0 "+ind2+"=ind2");            
     if(b){//если нашлась хотябы 1 скобка 
      //   plas0();//работает в скобках  
      if (ind2-ind1<3){ // в скобках 1 позиция
       System.out.println("перед удалением скобок,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
       setOper();
       vivod();
       deletBracket();//сносит скобки
       setOper();
       vivod();
      }
      Division();
      setOper();
      vivod();
      StToArr();
      bracketSearch();
      schot();           
      /* for (int id=oper.length-1;id>ind1;--id){//cдвиг массива для установки скобок 
      oper[id]=oper[id-1];
      }
      oper[ind1]="(";//установка скобок
      oper[ind1+4]=")";
      schot();
      */ 
     }
     else{//
      //      /*
      Division();// не нужно удалять  скобки( работает если скобок нет, не сносит скобки)
      vivod();
      setOper(); 
      StToArr();
      bracketSearch();
      schot();
      //              */
     }
    }
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   
   }                                   
   //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    
   for (int i=ind1;i<(ind2);++i){ // ищем внутри границ счета сложение вычитание
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2
    if (oper[i].equals("+")){System.out.println("найдена операция +");
     if(b){//если нашлась хотябы 1 скобка 
      //   plas0();//работает в скобках  
      if (ind2-ind1<3){ // в скобках 1 позиция
       System.out.println("перед удалением скобок,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
       setOper();
       vivod();
       deletBracket();//сносит скобки
       setOper();
       vivod();
      }
      plas();
      setOper();
      vivod();
      StToArr();
      bracketSearch();
      schot();           
      /* for (int id=oper.length-1;id>ind1;--id){//cдвиг массива для установки скобок 
      oper[id]=oper[id-1];
      }
      oper[ind1]="(";//установка скобок
      oper[ind1+4]=")";
      schot();
      */ 
     }
     else{//
     //      /*
     plas();// не нужно удалять  скобки( работает если скобок нет, не сносит скобки)
     vivod();
     setOper(); 
     StToArr();
     bracketSearch();
     schot();
     //              */
    }
   }
   //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   //############################################################################################ 
   if (oper[i].equals("-")){System.out.println("найдена операция -");
    if(b){//если нашлась хотябы 1 скобка 
     //   plas0();//работает в скобках  
     if (ind2-ind1<3){ // в скобках 1 позиция
      System.out.println("перед удалением скобок,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
      setOper();
      vivod();
      deletBracket();//сносит скобки
      setOper();
      vivod();
     }
     mines();
     setOper();
     vivod();
     StToArr();
     bracketSearch();
     schot();           
     /* for (int id=oper.length-1;id>ind1;--id){//cдвиг массива для установки скобок 
     oper[id]=oper[id-1];
     }
     oper[ind1]="(";//установка скобок
     oper[ind1+4]=")";
     schot();
     */
    }
    else{//
     //      /*
     mines();// не нужно удалять  скобки( работает если скобок нет, не сносит скобки)
     vivod();
     setOper(); 
     StToArr();
     bracketSearch();
     schot();
     //              */
    }
   }  
  }                       
 }

 void SIN(){System.out.println("in SIN b="+b);
  if (b){
   // oper[ind1]=oper[ind1]+oper[ind2];перевезти из стринг в дабл*****************************************
   double a1=Double.parseDouble(oper[ind0+1]);
   double radians1 = Math.toRadians(a1);
   oper[ind0]= Double.toString(Math.sin(radians1));
   for(int j=ind0+1;j<ind0+2;++j){//  сдвигаем элементы массива на 1  через 1 элемента для коррекции длинны массива после выполнения операции
    oper[j]=oper[j+1];
   }  
   for (int ik=ind0+2;ik<(oper.length-2);ik++){ //коррекция длинны остального массива на число + операцию
    for (int l=1;l>0;--l){
     oper[l+ik-1]=oper[l+ik];
    }
   }
   ind2=ind2-1; 
   ind3= ind3-1;
  }
  else{ 
   double a1=Double.parseDouble(oper[ind0+1]);
   double radians1 = Math.toRadians(a1);
   oper[ind0]= Double.toString(Math.sin(radians1));
   for (int ik=ind0+1;ik<(oper.length-1);ik++){ //коррекция длинны остального массива на число + операцию
    for (int l=1;l>0;--l){
     oper[l+ik-1]=oper[l+ik];
    }
   }
   ind2=ind2-1; 
   ind3=ind3-1;
  }                                                                  
 }

 void Sqr(){System.out.println("in Sqr b="+b);
  if (b){
   // oper[ind1]=oper[ind1]+oper[ind2];перевезти из стринг в дабл*****************************************
   double a1=Double.parseDouble(oper[ind0+1]);
   oper[ind0]= Double.toString(Math.sqrt(a1));
   for(int j=ind0+1;j<ind0+2;++j){//  сдвигаем элементы массива на 1  через 1 элемента для коррекции длинны массива после выполнения операции
    oper[j]=oper[j+1];
   }  
   for (int ik=ind0+2;ik<(oper.length-2);ik++){ //коррекция длинны остального массива на число + операцию
    for (int l=1;l>0;--l){
     oper[l+ik-1]=oper[l+ik];
    }
   }
   ind2=ind2-1; 
   ind3= ind3-1;
  }
  else{ 
   double a1=Double.parseDouble(oper[ind0+1]);
   oper[ind0]= Double.toString(Math.sqrt(a1));
   for (int ik=ind0+1;ik<(oper.length-1);ik++){ //коррекция длинны остального массива на число + операцию
    for (int l=1;l>0;--l){
     oper[l+ik-1]=oper[l+ik];
    }
   }
   ind2=ind2-1; 
   ind3=ind3-1;
  }                                                                  
 }

 void Division(){// не нужно удалять  скобки( работает если скобоки  есть, не сносит скобки)
  System.out.println("метод division");// 
  if (b){
   double a1=Double.parseDouble(oper[ind0+1]);//работает если скобоки  есть
   double a2=Double.parseDouble(oper[ind0+3]);
   try 
    {
    oper[ind0+1]= Double.toString(a1/a2);//worked with int
    for(int j=ind0+2;j<ind0+4;++j){//  сдвигаем элементы массива на 1  через 2 элемента для коррекции длинны массива после выполнения операции
     oper[j]=oper[j+2];
    }  
    for (int ik=ind0+2;ik<(oper.length-2);ik++){ //коррекция длинны остального массива на число + операцию
     for (int l=2;l>0;--l){
      oper[l+ik-1]=oper[l+ik];
     }
    }
    ind2=ind2-2; 
    ind3= ind3-2;
   }
    catch(ArithmeticException e)
     {
     System.out.println("Division zero"); 
    }
    finally
     {
     System.out.println("Деление прошло");	
    }
  }
  else{
   double a1=Double.parseDouble(oper[ind0+1]);
   double a2=Double.parseDouble(oper[ind0+3]);
   oper[ind0+1]= Double.toString(a1/a2);
   for (int ik=ind0+2;ik<(oper.length-2);ik++){ //коррекция длинны остального массива на число + операцию
    for (int l=2;l>0;--l){
     oper[l+ik-1]=oper[l+ik];
    }
   }
   ind2=ind2-2; 
   ind3=ind3-2;
  }
 }

void mines(){
 if (b){
  // oper[ind1]=oper[ind1]+oper[ind2];перевезти из стринг в дабл*****************************************
  double a1=Double.parseDouble(oper[ind1+1]);//работает если скобоки  есть
  double a2=Double.parseDouble(oper[ind1+3]);
  oper[ind1+1]= Double.toString(a1-a2);
  /* for (int ik=ind1;ik<(oper.length);ik++){
  //  сдвигаем элементы массива на 1 (5 раза)           
  if ((ik==ind1)&&(ik<oper.length-5)){
  for(int j=0;j<6;++j){
  oper[ik+j]=oper[ik+j+3];
  }    
  } 
  }*/
  for(int j=ind1+2;j<ind1+4;++j){//  сдвигаем элементы массива на 1  через 2 элемента для коррекции длинны массива после выполнения операции
   oper[j]=oper[j+2];
  }  
  for (int ik=ind1+2;ik<(oper.length-2);ik++){ //коррекция длинны остального массива на число + операцию
   for (int l=2;l>0;--l){
    oper[l+ik-1]=oper[l+ik];
   }
  }
  ind2=ind2-2; 
  ind3= ind3-2;
 }
 else{ 
 double a1=Double.parseDouble(oper[ind1]);
 double a2=Double.parseDouble(oper[ind1+2]);
 oper[ind1]= Double.toString(a1-a2);
 for (int ik=ind1+1;ik<(oper.length-2);ik++){ //коррекция длинны остального массива на число + операцию
  for (int l=2;l>0;--l){
   oper[l+ik-1]=oper[l+ik];
   }
  }
  ind2=ind2-2; 
  ind3=ind3-2;
 }
}

void plas(){// не нужно удалять  скобки( работает если скобоки  есть, не сносит скобки)
 System.out.println("метод plus");// 
 if (b){
  // oper[ind1]=oper[ind1]+oper[ind2];перевезти из стринг в дабл*****************************************
  double a1=Double.parseDouble(oper[ind1+1]);//работает если скобоки  есть
  double a2=Double.parseDouble(oper[ind1+3]);
  oper[ind1+1]= Double.toString(a1+a2);
  /* for (int ik=ind1;ik<(oper.length);ik++){
  //  сдвигаем элементы массива на 1 (5 раза)           
  if ((ik==ind1)&&(ik<oper.length-5)){
  for(int j=0;j<6;++j){
  oper[ik+j]=oper[ik+j+3];
  }    
  } 
  }*/
  for(int j=ind1+2;j<ind1+4;++j){//  сдвигаем элементы массива на 1  через 2 элемента для коррекции длинны массива после выполнения операции
   oper[j]=oper[j+2];
  }  
  for (int ik=ind1+2;ik<(oper.length-2);ik++){ //коррекция длинны остального массива на число + операцию
   for (int l=2;l>0;--l){
    oper[l+ik-1]=oper[l+ik];
   }
  }
  ind2=ind2-2; 
  ind3= ind3-2;
  }
  else{ 
  double a1=Double.parseDouble(oper[ind1]);
  double a2=Double.parseDouble(oper[ind1+2]);
  oper[ind1]= Double.toString(a1+a2);
  for (int ik=ind1+1;ik<(oper.length-2);ik++){ //коррекция длинны остального массива на число + операцию
   for (int l=2;l>0;--l){
    oper[l+ik-1]=oper[l+ik];
   }
  }
  ind2=ind2-2; 
  ind3=ind3-2;
 }
}
void mul(){// не нужно удалять  скобки( работает если скобоки  есть, не сносит скобки)
 System.out.println("метод mul");// 
 if (b){
  // oper[ind1]=oper[ind1]+oper[ind2];перевезти из стринг в дабл*****************************************
  double a1=Double.parseDouble(oper[ind0+1]);//работает если скобоки  есть
  double a2=Double.parseDouble(oper[ind0+3]);
  oper[ind0+1]= Double.toString(a1*a2);
  /* for (int ik=ind1;ik<(oper.length);ik++){
  //  сдвигаем элементы массива на 1 (5 раза)           
  if ((ik==ind1)&&(ik<oper.length-5)){
  for(int j=0;j<6;++j){
  oper[ik+j]=oper[ik+j+3];
  }    
  } 
  }*/
  for(int j=ind0+2;j<ind0+4;++j){//  сдвигаем элементы массива на 1  через 2 элемента для коррекции длинны массива после выполнения операции
   oper[j]=oper[j+2];
  }  
  for (int ik=ind0+2;ik<(oper.length-2);ik++){ //коррекция длинны остального массива на число + операцию
   for (int l=2;l>0;--l){
    oper[l+ik-1]=oper[l+ik];
   }
  }
  ind2=ind2-2; 
  ind3= ind3-2;
 }
 else{ 
  double a1=Double.parseDouble(oper[ind0+1]);
  double a2=Double.parseDouble(oper[ind0+3]);
  oper[ind0+1]= Double.toString(a1*a2);
  for (int ik=ind0+2;ik<(oper.length-2);ik++){ //коррекция длинны остального массива на число + операцию
   for (int l=2;l>0;--l){
    oper[l+ik-1]=oper[l+ik];
   }
  }
  ind2=ind2-2; 
  ind3=ind3-2;
 }
}

void deletBracket(){    
 System.out.println(" in deletBracket()");
 //////////////////////////////////////////сносим скобки                               
 //  сдвигаем элементы массива на 1 для коррекции длинны массива после выполнения операции
 //      oper[ind1]=oper[ind1+1];
 for (int ik=ind1;ik<(ind3);ik++){ //коррекция длинны остального массива на число + операцию
  oper[ik]=oper[ik+1];
  //   oper[ik+1]=oper[ik+2];
  oper[ind2+(ik-ind1)]=oper[ind2+(ik-ind1)+1];  
  //  oper[ik+3]=oper[ik+4];
 }
 oper[ind3]="0";                                           
 ind2=ind2-2;
 ind3=ind3-2;
 if (ind1>0){ --ind1 ;}
 //////////////////////////////////////////////////////////////////
 b=false;
}
void vivod(){                   
 for (int i=0;i<(oper.length);i++){ //отображаем элементы массива
  System.out.println ("newoper["+i+"]"+oper[i]);
 }
}

void setOper(){
 s12="";
 for(int i =0;i<=(ind3);i++){
  s12+=" ";
  s12+=oper[i];
 }
 System.out.println("s12"+s12+"  "+ +ind1+" - ind1");
 input.setText(s12);
}

}
class SetSpise{
 char[] temp;
 boolean a= false ;
 String s12 ;
 public SetSpise(String s12){
  this.s12=s12;
 }
 String setSpise(String s12){
// this.s12=s12;
 System.out.println(s12);
 temp = s12.toCharArray();
 s12="";
 for (int i=0;i<temp.length;i++){
	  if(((temp[i]>='0')&&(temp[i]<='9'))||(temp[i]=='.')||(temp[i]==',')){
 		   s12+=temp[i];
		   for(int j=i+1;j<temp.length;j++){
		    if (((temp[j]>='0')&&(temp[j]<='9'))||(temp[j]=='.')||(temp[j]==',')) {
		     s12+=temp[j];
		     i++;
		    }
		    else{s12+=' '; break;}
		    }
	   }
  
   if((temp[i]>='A')&&(temp[i]<='z')||(temp[i]=='+')){
   s12+=temp[i];
//   i++;
    for(int j=i+1;j<temp.length;j++){
     if (((temp[j]>='A')&&(temp[j]<='z'))) {
      s12+=temp[j];
      i++;
     }else{ break;}    
    } 
    s12+=' ';
   }
  }
  System.out.println("\n2: "+s12);
  return s12;
 }
}
 class ButtonEventListener0 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += "0";
   input.setText(s12);
  }
 }

 class ButtonEventListener1 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += "1";
   input.setText(s12);
  }
 }

 class ButtonEventListener2 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += "2";
   input.setText(s12);
  }
 }

 class ButtonEventListener3 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += "3";
   input.setText(s12);
  }
 }
 class ButtonEventListener4 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += "4";
   input.setText(s12);
  }
 }

 class ButtonEventListener5 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += "5";
   input.setText(s12);
  }
 }
 class ButtonEventListener6 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += "6";
   input.setText(s12);
  }
 }
 class ButtonEventListener7 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += "7";
   input.setText(s12);
  }
 }
 class ButtonEventListener8 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += "8";
   input.setText(s12);
  }
 }
 class ButtonEventListener9 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += "9";
   input.setText(s12);
  }
 }
 class ButtonEventListenerDote implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += ".";
   input.setText(s12);
  }
 }
 class ButtonEventListenerZ1 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += " ( ";
   input.setText(s12);
  }
 }
 class ButtonEventListenerZ2 implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += " ) ";
   input.setText(s12);
  }
 }
 class ButtonEventListenerMul implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += " * ";
   input.setText(s12);
  }
 }
 class ButtonEventListenerDivision implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += " / ";
   input.setText(s12);
  }
 }
 class ButtonEventListenerClear implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 = "";
   input.setText(s12);
  }
 }
 class ButtonEventListenerMines implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += " - ";
   input.setText(s12);
  }
 }
 class ButtonEventListenerSIN implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += " SIN ";
   input.setText(s12);
  }
 }
 class ButtonEventListenerPlus implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += " + ";
   input.setText(s12 );
  }
 }

 class ButtonEventListenerSqr implements ActionListener{

@Override
  public void actionPerformed (ActionEvent e) {
   s12 += " Sqr ";
   input.setText(s12);
  }
 }
}
